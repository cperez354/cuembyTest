(function () {
    'use strict';

    angular
        .module('cuemby')
        .service('homeService', homeService)

    homeService.$inject = ['$q', '$http', 'constantService']

    /** @ngInject */
    function homeService($q, $http, constantService) {

        var vm = this;

        /**
         * Method to get students from server.
         * @returns {Promise}
         */
        vm.getStudents = function () {
            var deferred = $q.defer();
            $http.get(constantService.getUrlStudents(), {
                cache: false,
            })
                .success(function (data) {
                    deferred.resolve({error: false, students: data});
                })
                .error(function (err) {
                    deferred.resolve({error: true, e: err});
                });
            return deferred.promise;
        };

        return {
            getStudents: vm.getStudents
        };

    }

}());