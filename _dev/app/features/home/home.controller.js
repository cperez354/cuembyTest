(function () {
    'use strict';

    angular
        .module('cuemby')
        .controller('homeController', homeController)

    homeController.$injejct = ['homeService', '$rootScope'];

    function homeController(homeService, $rootScope) {
        var vm = this;
        vm.students = [];
        vm.detailOpen = false;
        vm.selectStudent = undefined;

        /**
         * Method to get students with homeService.
         */
        vm.getStudents = function () {
            homeService.getStudents().then(function (data) {
                if (data.error) {
                    alert($rootScope.root_messages.general.errorstudents);
                }
                else {
                    vm.students = data.students;
                    console.log(vm.students);
                }

            }, function (reason) {
                alert('Failed: ' + reason);
            });

        };

        /**
         * Method to Select student and open details.
         * @param $index
         */
        vm.setselectStudent = function ($index) {
            vm.selectStudent = vm.students[$index];
            vm.average();
        };

        /**
         * Method to hide student details.
         */
        vm.hideSelectedStudent = function () {
            vm.selectStudent = undefined;
        };

        /**
         * Method to calculate the average of grades in student.
         */
        vm.average = function () {
            if (vm.selectStudent.grades.length > 0) {
                var average = 0;
                vm.selectStudent.grades.map(function (number) {
                    average = average + number;
                });
                vm.selectStudent.average = average / vm.selectStudent.grades.length;
            }
        };

        /**
         * Call to getStudents().
         */
        vm.getStudents();
    }

}());