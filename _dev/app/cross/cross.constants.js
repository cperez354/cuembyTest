(function () {
    "use strict";

    angular
        .module('cuemby')
        .factory('constantService', constantService);

    constantService.$inject = [];

    function constantService() {

        var vm = this;

        /**
         * Function thats returns de string api endpoint for students.
         * @returns {string}
         */
        vm.getUrlStudents = function () {
            return '/api/students';
        };

        return {
            server: vm.endponint,
            getUrlStudents: vm.getUrlStudents
        }

    }
})();
