(function () {
    "use strict";

    angular
        .module('cuemby')
        .factory('messagesProvider', messagesProvider);

    messagesProvider.$inject = ['$q', '$http'];

    function messagesProvider($q, $http) {

        var vm = this;

        /**
         * Method to get messages.
         * @returns {Promise}
         */
        vm.getMessages = function() {
            var deferred = $q.defer();
            $http.get('../../strings_en.json', {
                cache: true
            }).success(function (data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        }

        return {
            getMessages: vm.getMessages
        }
    }
})();