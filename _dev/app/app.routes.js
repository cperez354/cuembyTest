"use strict";

angular
    .module('cuemby')
    .config(routes);

routes.$inject = ['$stateProvider', '$urlRouterProvider'];

function routes($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'app/features/home/home.view.html',
            controller: 'homeController as home'
        });

    $urlRouterProvider.otherwise('/')
}