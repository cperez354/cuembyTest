"use strict";

angular
    .module('cuemby')
    .run(run);

run.$inject = ['$rootScope', 'messagesProvider'];

function run($rootScope, messagesProvider) {

    /**
     * @Description
     * Update rootScope and asign root messages from a service
     */
    messagesProvider.getMessages().then(function (data) {
        $rootScope.root_messages = data;
    });

}