import express from 'express'

const router = express.Router()

/**
 * Provisional bd
 * @type {[*]}
 */
let students = [{
    "id": 101,
    "name": "Carlos",
    "active": true,
    "grades": [2.3, 4.3, 5.0],
    "yearsOld": 15,
    "color": "blue"
}, {
    "id": 202,
    "name": "Mark",
    "active": true,
    "grades": [3.3, 5.0, 3.0],
    "yearsOld": 18,
    "color": "green"
}, {
    "id": 303,
    "name": "Stephany",
    "active": true,
    "grades": [3.3, 4.0, 3.8],
    "yearsOld": 17,
    "color": "pink"
}, {
    "id": 404,
    "name": "Angela",
    "active": true,
    "grades": [1.0, 4.8, 4.0],
    "yearsOld": 16,
    "color": "gray"
}, {
    "id": 505,
    "name": "Jackson",
    "active": false,
    "grades": [5.0, 5.0, 5.0],
    "yearsOld": 15,
    "color": "blue"
}, {
    "id": 606,
    "name": "Melissa",
    "active": false,
    "grades": [3.0, 4.0, 5.0],
    "yearsOld": 19,
    "color": "yellow"
}, {
    "id": 707,
    "name": "Mariana",
    "active": true,
    "grades": [4.1, 4.0, 4.3],
    "yearsOld": 19,
    "color": "purple"
}, {
    "id": 808,
    "name": "Argemiro",
    "active": true,
    "grades": [2.0, 2.9, 3.0],
    "yearsOld": 29,
    "color": "aqua"
}]


/**
 * GET /api/students
 */
router.get('/students', (req, res) => {
    res.json(students)
})

export default router