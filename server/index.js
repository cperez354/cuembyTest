import http from 'http'
import express from 'express'
import students from '../server/api'

const app = express()
const server = http.createServer(app)
const port = process.env.PORT || 3000

app.use(express.static('dist'))
app.use('/api', students)

server.listen(port, () => console.log(`Server in the port ${port}`))