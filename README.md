TEST CUEMBY
=====================

## Getting Started

Install npm dependencies

```bash
$ npm install
```

Install bower dependencies

```bash
$ bower install
```

Run project in a window of terminal

```bash
$ npm start
```
Run project watch sass for develop in another window of terminal

```bash
$ gulp
```

Test production app
[HERE](https://testcuemby.herokuapp.com)



